One-File-C++
============

Introduction
------------

One-File-C++ is a utility, which allows you to write the header and source
component of a C++ file in one file. The tool will extract the header
portion from your file and automatically generate a header during compilation.
The .cpp file you write is correct C++ code and will compile on it's own.

One-File-C++ is still in it's infancy and might eat your sourcecode (use
version control)!

What One-File-C++ Does
----------------------

* Extracts xyz.h from xyz.cpp.
* Automatically adds guards to the header
* Makes sure that the line numbers in the generated header correspond to
  the line numbers in your .cpp file, so that error messages and debugging
  work nicely with whatever you wrote
* Asserts that an existing header file is only overwritten if it contains
  a One-File-C++ guard


What One-File-C++ Does Not Do
-----------------------------

* Does not parse your cpp code (Just looks for two comment markers in the
  code)
* Does not translate or transform your code in any way
* Does not remove any redundancy between header and source file


Not Implemented, Yet
--------------------
    
* Allow to merge changes in the generated header file back into the
  original cpp file
* Make generated files read-only, so that your text editor can warn you
  when you try to modify the generated files (e.g. during debugging)


Installation
------------

No installation. Just put the file `one-file-cpp` in your path and make sure 
that python is installed.

How To Use One-File-C++
-----------------------

In your source file, add the two lines

```
#!c++
#define OFC_HEADER_MYFILENAME

```

and
    
```
#!c++
#define OFC_SOURCE

```  

The header marker indicates the beginning of the header and also serves as
a guard against indirect inclusion of the autogenerated header. It must
be preceded by an empty line! The source marker marks the end of the header.

Somewhere in your build process, add


```
#!shell
$ one-file-cpp *.cpp

```

You can define where the headers should go with the -o options:

```
#!shell
$ one-file-cpp src/ -o include/
```

One-File-C++ will try to replicate your directory structure in the header 
directory.

See Also
--------

If you want something that is more advanced, have a look at
[Lazy C++](http://www.lazycplusplus.com/). The latest release (as of writing
this) is from 2010, so it might have problems with C++11 and C++14 code.

